package pl.com.tt.logger.xml;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class XmlUnmarshaller {
	public <T> T unmarshall(File xmlFile, Class<?>... clazz) throws JAXBException{
		JAXBContext jaxbContext= JAXBContext.newInstance(clazz);
		Unmarshaller unmarshaller= jaxbContext.createUnmarshaller();
		T unmarshalledObj= (T) unmarshaller.unmarshal(xmlFile);
		
		return unmarshalledObj;
	}
}
