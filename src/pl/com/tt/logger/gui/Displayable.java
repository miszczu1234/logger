package pl.com.tt.logger.gui;

public interface Displayable {
	void display(String message, String colorName, int fontSize);
}
