package pl.com.tt.logger.gui;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class LabelDisplay implements Displayable{

	private VBox container;
	
	public LabelDisplay(VBox container) {
		this.container= container;
	}
	
	@Override
	public void display(String message, String colorName, int fontSize) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				Label label= new Label(message);
				label.setFont(new Font(fontSize));
				label.setMinWidth(Region.USE_PREF_SIZE);
				label.setTextFill(Color.web(colorName));
				container.getChildren().add(label);
			}
		});
	}
}
