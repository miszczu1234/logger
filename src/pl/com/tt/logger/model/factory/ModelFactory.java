package pl.com.tt.logger.model.factory;

import java.util.Arrays;

import org.apache.log4j.Logger;

import pl.com.tt.logger.model.LogBlock;
import pl.com.tt.logger.model.LogBlockWrapper;

public class ModelFactory {
	
	private static final Logger LOG= Logger.getLogger(ModelFactory.class);
	
	private static final int DATA_OBJ_CNT = 5;
	private static final String DATA_DELIM= " ";
	
	private String[] parseData(String data){
		
		String[] parsedData= new String[DATA_OBJ_CNT+1];
		
		for(int i=0; i<DATA_OBJ_CNT; i++){
			String value= data.substring(0, data.indexOf(DATA_DELIM)+1);
			parsedData[i]= value.trim();
			data= data.substring(data.indexOf(DATA_DELIM)+1).trim();
			LOG.debug(String.format("parseData : %s", data));
		}
		parsedData[DATA_OBJ_CNT]= data;
		
		return parsedData;
	}
	
	public LogBlockWrapper getLogBlock(String data){
		
		LogBlock dataBlock= new LogBlock();
		
		String[] parsedData= parseData(data);
		
		for(int i= 0; i< parsedData.length; i++){
			DataIndexMap indexMap= DataIndexMap.getByValue(i);
			if(indexMap== null){
				continue;
			}
			dataBlock.put(indexMap, new String(parsedData[i]));
		}
		LogBlockWrapper logBlockWrapper= new LogBlockWrapper(dataBlock);
		
		return logBlockWrapper;
	}
}
