package pl.com.tt.logger.model.factory;

public enum DataIndexMap {
	DATE(0),
	TIME(1),
	LOG_TYPE(2),
	LOG_CLASS(3),
	LOG_CLASS_2(4),
	TEXT(5);
	
	private int index;
	
	private DataIndexMap(int index) {
		this.index= index;
	}
	
	public static DataIndexMap getByValue(int index){
		
		for(DataIndexMap dataIndexMap : DataIndexMap.values()){
			if(dataIndexMap.index== index){
				return dataIndexMap;
			}
		}
		
		return null;
	}
}
