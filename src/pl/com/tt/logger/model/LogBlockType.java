package pl.com.tt.logger.model;

public enum LogBlockType {
	INFO,
	ERROR,
	DEBUG,
	WARRN,
	FATAL,
	OTHER;
	
	public static LogBlockType getByValue(String value){
		for(LogBlockType logBlockType : LogBlockType.values()){
			if(logBlockType.toString().equals(value)){
				return logBlockType;
			}
		}
		
		return LogBlockType.OTHER;
	}
}
