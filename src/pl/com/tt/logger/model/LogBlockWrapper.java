package pl.com.tt.logger.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import pl.com.tt.logger.model.factory.DataIndexMap;

public class LogBlockWrapper extends LogBlock{
	
	private static final Logger LOG= Logger.getLogger(LogBlockWrapper.class);
	
	private static final String CREATE_LOG_DATE_FORMAT = "yyyy-MM-dd hh:mm:ss,SSS";
	
	public LogBlockWrapper(LogBlock logBlock) {
		super();
		if((logBlock!=null) && (logBlock.getValues()!=null)){
			this.values= logBlock.getValues();
		}
	}
	
	public Date getCreateLogDate() throws ParseException{
		
		String strLogDate= values.get(DataIndexMap.DATE);
		String strTime= values.get(DataIndexMap.TIME);
		strLogDate= strLogDate+" "+strTime;
		SimpleDateFormat simpleDateFormat= new SimpleDateFormat(CREATE_LOG_DATE_FORMAT);
		Date logDate= simpleDateFormat.parse(strLogDate);
		
		return logDate;
	}
	
	public LogBlockType getLogBlockType(){
		String strLogBlockType= values.get(DataIndexMap.LOG_TYPE);
		LogBlockType logBlockType= LogBlockType.getByValue(strLogBlockType);
		
		return logBlockType;
	}
	
	public String getLogClass(){
		return values.get(DataIndexMap.LOG_CLASS);
	}
	
	public String getLogClass2(){
		return values.get(DataIndexMap.LOG_CLASS_2);
	}
	
	public String getText(){
		return values.get(DataIndexMap.TEXT);
	}
	
	public String put(DataIndexMap key, String value){
		return values.put(key, value);
	}
	
	public String get(DataIndexMap key){
		return values.get(key);
	}
	
	public void appendText(String text){
		String currText= values.get(DataIndexMap.TEXT);
		currText= currText.concat("\n"+text);
		values.put(DataIndexMap.TEXT, currText);
	}
	
	@Override
	public String toString() {
		StringBuilder resultStr= new StringBuilder("");
		DateFormat dateFormat= new SimpleDateFormat(CREATE_LOG_DATE_FORMAT);
		String strLogDate= "";
		try{
			strLogDate= dateFormat.format(getCreateLogDate());
		}catch(ParseException e){
			LOG.error(String.format("toString : Cannot parse date %s", values.get(DataIndexMap.DATE)));
		}
		resultStr.append(strLogDate)
					.append(" "+getLogBlockType())
					.append(" "+getLogClass())
					.append(" "+getLogClass2())
					.append(" "+getText());
		
		return resultStr.toString();
	}
}
