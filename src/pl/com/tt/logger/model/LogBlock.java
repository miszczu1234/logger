package pl.com.tt.logger.model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import pl.com.tt.logger.model.factory.DataIndexMap;

public class LogBlock {
	
	protected Map<DataIndexMap, String> values;
	
	public LogBlock(){
		values= new HashMap<>();
	}
	
	public String put(DataIndexMap key, String value){
		return values.put(key, value);
	}
	
	public String get(DataIndexMap key){
		return values.get(key);
	}
	
	@Override
	public String toString() {
		StringBuilder result= new StringBuilder("LogBlock:"+values.size()+"[\n");
		Iterator<Entry<DataIndexMap, String>> valuesIterator= values.entrySet().iterator();
		
		while(valuesIterator.hasNext()){
			Entry<DataIndexMap, String> valuesEntry= valuesIterator.next();
			result.append(String.format("%s=%s\n", valuesEntry.getKey(), valuesEntry.getValue()));
		}
		result.append("]");
		
		return result.toString();
	}
	
	public int getValuesSize(){
		return values.size();
	}

	public Map<DataIndexMap, String> getValues() {
		return values;
	}
}
