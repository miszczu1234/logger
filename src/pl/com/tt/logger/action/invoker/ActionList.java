package pl.com.tt.logger.action.invoker;

import pl.com.tt.logger.action.Action;

interface ActionList {
	public void add(Action action) throws InterruptedException;
	public Action get() throws InterruptedException;
}
