package pl.com.tt.logger.action.invoker;

import java.io.IOException;

import org.apache.log4j.Logger;

import pl.com.tt.logger.action.Action;

class ActionConsumer extends Thread{
	
	private static final Logger LOG= Logger.getLogger(ActionConsumer.class);
	
	private ActionList actionHandler;
	
	public ActionConsumer(ActionList defaultActionHandler) {
		this.actionHandler= defaultActionHandler;
	}
	
	@Override
	public void run() {
		
		LOG.info("Action consumer thread started");
		
		try{
			while(!Thread.currentThread().isInterrupted()){
				Action action= actionHandler.get();
				action.invoke();	
			}
		}catch(InterruptedException e){
			LOG.debug("run : thread interrupted");
		}
		
		LOG.info("Action consumer thread finish");
	}
}
