package pl.com.tt.logger.action.invoker;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import pl.com.tt.logger.action.Action;

class DefaultActionHandler implements ActionList{
	
	private static final int DEFAULT_CAPACITY = 20;
	
	private BlockingQueue<Action> actions;
	
	public DefaultActionHandler() {
		actions= new ArrayBlockingQueue<>(DEFAULT_CAPACITY, true);
	}
	
	public synchronized void add(Action action) throws InterruptedException{
		actions.put(action);
	}
	
	public Action get() throws InterruptedException{
		return actions.take();
	}
}
