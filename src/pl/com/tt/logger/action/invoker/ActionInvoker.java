package pl.com.tt.logger.action.invoker;

import pl.com.tt.logger.action.Action;

public class ActionInvoker {
	
	private ActionList actionList;
	private ActionConsumer actionConsumerThread;
	
	public ActionInvoker() {
		actionList= new DefaultActionHandler();
		actionConsumerThread= new ActionConsumer(actionList);
		actionConsumerThread.start();
	}
	
	public void addAction(Action action) throws InterruptedException{
		actionList.add(action);
	}
	
	public Action getAction() throws InterruptedException{
		return actionList.get();
	}
	
	public void interrupt(){
		if(actionConsumerThread.isAlive()){
			actionConsumerThread.interrupt();
		}
	}
}
