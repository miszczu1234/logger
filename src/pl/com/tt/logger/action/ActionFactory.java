package pl.com.tt.logger.action;

import javafx.scene.control.TextArea;
import pl.com.tt.logger.gui.Displayable;
import pl.com.tt.logger.model.LogBlock;
import pl.com.tt.logger.model.LogBlockWrapper;

public class ActionFactory {
	public Action getDisplayLogAction(Displayable displayArea, LogBlockWrapper logBlock){
		Action action= new DisplayLogAction(displayArea, logBlock);
		
		return action;
	}
}
