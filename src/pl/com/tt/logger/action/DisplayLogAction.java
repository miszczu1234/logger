package pl.com.tt.logger.action;

import org.apache.log4j.Logger;

import pl.com.tt.logger.gui.Displayable;
import pl.com.tt.logger.model.LogBlockType;
import pl.com.tt.logger.model.LogBlockWrapper;
import pl.com.tt.logger.property.PropertyHandler;
import pl.com.tt.logger.property.PropertyList;

public class DisplayLogAction implements Action{

	private static final Logger LOG= Logger.getLogger(DisplayLogAction.class);
	
	private static final String PROPERTY_PREFIX = "font.color.";
	
	private Displayable logArea;
	private LogBlockWrapper logBlock;
	
	public DisplayLogAction(Displayable displayable, LogBlockWrapper logBlock) {
		logArea= displayable;
		this.logBlock= logBlock;
	}
	
	private String getLogColor(){
		LogBlockType type= logBlock.getLogBlockType();
		String propertyKey= PROPERTY_PREFIX+type.toString();
		String color= "#"+PropertyHandler.getInstance().getProperty(propertyKey);
		
		return color;
	}
	
	@Override
	public void invoke() {
		String strLog= logBlock.toString();
		String logColor= getLogColor();
		int fontSize= Integer.parseInt(PropertyHandler.getInstance().getProperty(PropertyList.FONT_SIZE_DEFAULT));
		logArea.display(strLog, logColor, fontSize);
	}
}
