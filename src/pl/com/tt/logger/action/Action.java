package pl.com.tt.logger.action;

public interface Action {
	public void invoke();
}
