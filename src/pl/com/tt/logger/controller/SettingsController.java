package pl.com.tt.logger.controller;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.sun.prism.image.ViewPort;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import pl.com.tt.logger.config.Models;
import pl.com.tt.logger.property.PropertyHandler;
import pl.com.tt.logger.util.LogUtilities;
import pl.com.tt.logger.view.config.ViewHandler;
import pl.com.tt.logger.view.config.model.PreferenceModel;
import pl.com.tt.logger.view.config.model.ViewModel;
import pl.com.tt.logger.view.factory.ContainersFactory;

public class SettingsController implements Initializable{
	
	private static final Logger LOG= Logger.getLogger(SettingsController.class);
	
	//fxml properties
	@FXML
    private ListView<String> listPreferences;
	@FXML
    private BorderPane viewContainer;
	
	private ViewHandler viewHandler;
	private ViewModel prefViewModel;
	
	public SettingsController() {
		
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		viewHandler= new ViewHandler();
		prefViewModel= viewHandler.getModel(Models.PREF_MODEL);
		LOG.debug(String.format("loadView : view loaded %s", prefViewModel));
		List<String> prefNames= prefViewModel.getPreferencesNames();
		LOG.debug("initialize : names="+Arrays.toString(prefNames.toArray()));
		listPreferences.setItems(FXCollections.observableList(prefNames));
		listPreferences.getSelectionModel().selectedItemProperty().addListener(new PreferenceListChangeListerner());
		listPreferences.getSelectionModel().select(0);
	}

	private void closeOnButton(ActionEvent event){
		Button closeBtn= (Button)event.getSource();
		((Stage)closeBtn.getScene().getWindow()).close();
	}
	
	@FXML
	public void saveProperties(ActionEvent event){
		PropertyHandler.getInstance().saveTmpProperties();
		closeOnButton(event);
	}
	
	@FXML
	public void rollbackProperties(ActionEvent event){
		PropertyHandler.getInstance().clearTmpProperties();
		closeOnButton(event);
	}
	
	private class PreferenceListChangeListerner implements ChangeListener<String>{
		
		private ContainersFactory containersFactory;
		
		public PreferenceListChangeListerner() {
			containersFactory= new ContainersFactory();
		}
		
		private void setView(PreferenceModel prefModel){
			try{
				String viewPath= prefModel.getView();
				LOG.debug(String.format("changed : try change view on : %s", viewPath));
				AnchorPane contentPane= containersFactory.getContainer(viewPath);
				viewContainer.setCenter(contentPane);
			}catch(IOException e){
				LOG.error(String.format("changed : cannot set view\n%s", LogUtilities.stackTraceAsString(e)));
			}
		}
		
		@Override
		public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
			for(PreferenceModel prefModel : prefViewModel.getPreferences()){
				if(prefModel.getName().equals(newValue)){
					setView(prefModel);
				}
			}
		}
		
	}
	
}
