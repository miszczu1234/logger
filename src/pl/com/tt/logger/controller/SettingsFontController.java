package pl.com.tt.logger.controller;

import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.paint.Color;
import pl.com.tt.logger.property.PropertyHandler;
import pl.com.tt.logger.property.PropertyList;

public class SettingsFontController implements Initializable{
	
	private static final Logger LOG= Logger.getLogger(SettingsFontController.class);
	
	@FXML
    private ColorPicker colorErr;

    @FXML
    private ComboBox<Integer> fontSize;

    @FXML
    private ColorPicker colorWarn;

    @FXML
    private ColorPicker colorDebug;

    @FXML
    private ColorPicker colorInfo;
    
    @FXML
    private ColorPicker colorFatal;

    private ObservableList<Integer> getAvailableFontSize(){
    	List<Integer> availableFontSize= new LinkedList<>();
    	Integer minSize= Integer.parseInt(
    			PropertyHandler.getInstance().getProperty(PropertyList.FONT_SIZE_MIN));
    	Integer maxSize= Integer.parseInt(
    			PropertyHandler.getInstance().getProperty(PropertyList.FONT_SIZE_MAX));
    	for(Integer i= minSize; i<= maxSize; i++){
    		availableFontSize.add(i);
    	}
    	
    	return FXCollections.observableArrayList(availableFontSize);
    }
    
    private void setSizeList(){
    	ObservableList<Integer> availableFontSize= getAvailableFontSize();
    	fontSize.setItems(availableFontSize);
    	Integer defaultSize= Integer.parseInt(
    			PropertyHandler.getInstance().getProperty(PropertyList.FONT_SIZE_DEFAULT));
    	fontSize.setValue(defaultSize);
    	fontSize.setOnAction(new FontSizeChangedEventListener());
    }
    
    private void setColor(ColorPicker picker, String hexColor){
    	picker.setValue(Color.web("#"+hexColor));
    }
    
    private void setColorChangedAction(ColorPicker colorPicker, String colorContextPref){
    	colorPicker.setOnAction(new ColorFontChangedEventListener(colorContextPref));
    }
    
    private void setValuesFromProperties(){
    	//set fonts colors
    	String errColor= PropertyHandler.getInstance().getProperty(PropertyList.COLOR_ERROR);
    	setColor(colorErr, errColor);
    	setColorChangedAction(colorErr, PropertyList.COLOR_ERROR.getName());
    	
    	String debColor= PropertyHandler.getInstance().getProperty(PropertyList.COLOR_DEBUG);
    	setColor(colorDebug, debColor);
    	setColorChangedAction(colorDebug, PropertyList.COLOR_DEBUG.getName());
    	
    	String infColor= PropertyHandler.getInstance().getProperty(PropertyList.COLOR_INFO);
    	setColor(colorInfo, infColor);
    	setColorChangedAction(colorInfo, PropertyList.COLOR_INFO.getName());
    	
    	String warColor= PropertyHandler.getInstance().getProperty(PropertyList.COLOR_WARN);
    	setColor(colorWarn, warColor);
    	setColorChangedAction(colorWarn, PropertyList.COLOR_WARN.getName());
    	
    	String fatColor= PropertyHandler.getInstance().getProperty(PropertyList.COLOR_FATAL);
    	setColor(colorFatal, fatColor);
    	setColorChangedAction(colorFatal, PropertyList.COLOR_FATAL.getName());
    	
    	//set font size
    	setSizeList();
    }
    
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		LOG.debug("SettingFontController initialize");
		setValuesFromProperties();
	}
	
	private class ColorFontChangedEventListener implements EventHandler<ActionEvent>{

		private String prefName;
		
		public ColorFontChangedEventListener(String prefName) {
			this.prefName= prefName;
		}
		
		public String toWebColor(Color color){
	        return String.format("%02X%02X%02X",
	            (int)(color.getRed()*255),
	            (int)(color.getGreen()*255),
	            (int)(color.getBlue()*255));
	    }
		
		@Override
		public void handle(ActionEvent event) {
			ColorPicker colorPicker= (ColorPicker)event.getSource();
			Color selectedColor= colorPicker.getValue();
			String webColor= toWebColor(selectedColor);
			PropertyHandler.getInstance().setTmpProperty(prefName, webColor);
		}
		
	}
	
	private class FontSizeChangedEventListener implements EventHandler<ActionEvent> {

		@SuppressWarnings("unchecked")
		@Override
		public void handle(ActionEvent event) {
			ComboBox<Integer> fontSizeComboBox = (ComboBox<Integer>) event.getSource();
			int selectedFontSize = fontSizeComboBox.getValue();
			PropertyHandler.getInstance().setTmpProperty(PropertyList.FONT_SIZE_DEFAULT.getName(),
					String.valueOf(selectedFontSize));
		}

	}
}
