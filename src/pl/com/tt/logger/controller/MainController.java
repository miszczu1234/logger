package pl.com.tt.logger.controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import pl.com.tt.logger.action.invoker.ActionInvoker;
import pl.com.tt.logger.config.Views;
import pl.com.tt.logger.file.ChangeListener;
import pl.com.tt.logger.file.factory.FileChangeListenerFactory;
import pl.com.tt.logger.gui.Displayable;
import pl.com.tt.logger.gui.LabelDisplay;
import pl.com.tt.logger.util.LogUtilities;
import pl.com.tt.logger.view.factory.AlertFactory;
import pl.com.tt.logger.view.manager.ViewStarter;

public class MainController implements Initializable{

	//constants
	private static final Logger LOG= Logger.getLogger(MainController.class);
	
	//FXML properties
	@FXML
    private VBox vboxLogContainer;
	
    //properties
    private ChangeListener logDirListener;
    private Displayable displayable;
    private File logDirectory;
    
    //
    private ActionInvoker actionInvoker;
    private FileChangeListenerFactory fileChangeListenerFactory;
    private ViewStarter viewStarter;
    
    public MainController() {
    	
	}
    
    private void interruptThread(Thread thread){
    	if((thread!=null) && (thread.isAlive())){
    		thread.interrupt();
    	}
    }
    
    private void exit(){
    	interruptThread(logDirListener);
    	if(actionInvoker!=null){
    		actionInvoker.interrupt();
    	}
    	Platform.exit();
    }
    
    private void showSelectDirDialog(){
    	Alert alert= AlertFactory.getAlert(AlertType.INFORMATION, "Please select folder with logs", "ATTENTION", ButtonType.OK);
    	alert.showAndWait();
    }
    
    private boolean validateSelectedFolder(File directory){
    	if(directory!=null){
    		return true;
    	}
    	Alert alert= AlertFactory.getAlert(AlertType.ERROR, "Please select logs folder", "Select logs folder", ButtonType.OK, ButtonType.CLOSE);
    	alert.showAndWait();
    	ButtonType result= alert.getResult();
    	if(result.equals(ButtonType.OK)){
    		return showFileChooser();
    	}
    	
    	return false;
    }
    
    private boolean showFileChooser(){
    	DirectoryChooser chooser = new DirectoryChooser();
    	chooser.setTitle("JavaFX Projects");
    	File defaultDirectory = new File("c:/");
    	chooser.setInitialDirectory(defaultDirectory);
    	logDirectory = chooser.showDialog(null);
    	if(!validateSelectedFolder(logDirectory)){
    		exit();
    		return false;
    	}
    	
    	return true;
    }
    
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		viewStarter= new ViewStarter();
		actionInvoker= new ActionInvoker();
    	fileChangeListenerFactory= new FileChangeListenerFactory();
    	displayable= new LabelDisplay(vboxLogContainer);
    	showSelectDirDialog();
    	if(!showFileChooser()){
    		return;
    	}
    	try{
        	logDirListener= fileChangeListenerFactory.getDirectoryLogListener(logDirectory.getAbsolutePath(), actionInvoker, displayable);
        	logDirListener.start();
    	}catch(IOException e){
    		LOG.error("Cannot create listener thred");
    	}
	}
	
	@FXML
    void actionMenuFileClose(ActionEvent event) {
		exit();
    }
	
	@FXML
    void actionMenuViewPref(ActionEvent event) {
		try {
			viewStarter.showView(Views.SETTINGS_VIEW, "Application Preferences");
		} catch (IOException e) {
			LOG.debug(String.format("actionMenuViewPref : cannot create view\n%s", LogUtilities.stackTraceAsString(e)));
			Alert alert= AlertFactory.getAlert(AlertType.ERROR, "Cannot open view, occured exception\n"+e.getMessage(), 
					"View Starter Error", ButtonType.OK);
			alert.showAndWait();
		}
    }
}