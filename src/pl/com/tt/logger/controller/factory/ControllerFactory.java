package pl.com.tt.logger.controller.factory;

import java.net.URL;

import javafx.fxml.FXMLLoader;

public class ControllerFactory {
	
	public <T> T getContoller(String viewPath){
		URL viewUrl= getClass().getClassLoader().getResource(viewPath);
		FXMLLoader loader= new FXMLLoader(viewUrl);
		T controller =loader.getController();
		
		return controller;
	}
}
