package pl.com.tt.logger.file.factory;

import java.io.IOException;

import javafx.scene.control.TextArea;
import pl.com.tt.logger.action.invoker.ActionInvoker;
import pl.com.tt.logger.file.ChangeListener;
import pl.com.tt.logger.file.FileChangeListener;
import pl.com.tt.logger.file.LogDirectoryChangeListener;
import pl.com.tt.logger.gui.Displayable;

public class FileChangeListenerFactory {
	
	public FileChangeListener getListener(String filename, ActionInvoker actionInvoker, Displayable displayArea) throws IOException{
		
		FileChangeListener fileChangeListener = new FileChangeListener(filename, actionInvoker, displayArea);
		
		return fileChangeListener;
	}
	
	public ChangeListener getDirectoryLogListener(String filename, ActionInvoker actionInvoker, Displayable displayArea) throws IOException{
		LogDirectoryChangeListener logDirChangeListener= new LogDirectoryChangeListener(filename, actionInvoker, displayArea);
		
		return logDirChangeListener;
	}
}
