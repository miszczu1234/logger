package pl.com.tt.logger.file.refresh;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

public class Touch extends Thread {
	
	private static final Logger LOG= Logger.getLogger(Touch.class);
	
	private static final long REFRESH_TIME = 1000L;
	
	private List<Path> toRefresh;
	
	public Touch(List<Path> toRefresh) {
		this.toRefresh= toRefresh;
	}
	
	private void touches(){
		for(Path filePath : toRefresh){
			touch(filePath);
		}
	}
	
	private void touch(Path file) {
        try{
        	if (Files.exists(file)) {
            	long timestamp = System.currentTimeMillis();
                FileTime ft = FileTime.fromMillis(timestamp);
                Files.setLastModifiedTime(file, ft);
            }
        }catch(IOException e){
        	LOG.error(String.format("touch : Cannot refresh file : %s", file));
        }
		
    }

	@Override
	public void run() {
		try{
			while(!Thread.currentThread().isInterrupted()){
				Thread.sleep(REFRESH_TIME);
				touches();
			}
		}catch(InterruptedException e){
			LOG.debug("run : refresh thread interrupted");
		}
	}
}
