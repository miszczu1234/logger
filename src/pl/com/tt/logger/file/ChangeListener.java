package pl.com.tt.logger.file;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;

import org.apache.log4j.Logger;

import com.sun.nio.file.SensitivityWatchEventModifier;

import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.List;

import pl.com.tt.logger.file.util.FileUtil;
import pl.com.tt.logger.loggerwatchservice.factory.WatchServiceFactory;

public abstract class ChangeListener extends Thread{
	
	private static final Logger LOG= Logger.getLogger(ChangeListener.class);
	
	private String filename;
	
	protected WatchService watchService;
	private WatchServiceFactory watchServiceFactory;
	protected List<Kind<Path>> events;
	
	public ChangeListener(String filename, List<Kind<Path>> events) throws IOException{
		this.filename= filename;
		watchServiceFactory= new WatchServiceFactory();
		watchService= watchServiceFactory.getWatchService();
		this.events= events;
	}
	
	protected abstract void handleEvent(WatchEvent<?> eventType, Path context) throws InterruptedException;
	
	protected void observerEvents(WatchKey watchKey) throws InterruptedException{
		for(WatchEvent<?> event : watchKey.pollEvents()){
			Path contextPath= (Path) event.context();
			handleEvent(event, contextPath);
		}
	}
	
	@Override
	public void run() {
		
		Path filePath= FileSystems.getDefault().getPath(filename);
		LOG.debug(String.format("run : new change listener started, file %s", filePath));
		
		try {
			filePath.register(watchService, events.toArray(new Kind[events.size()]), SensitivityWatchEventModifier.HIGH);
			while(!Thread.currentThread().isInterrupted()){
				WatchKey localWk= watchService.take();
				observerEvents(localWk);
				localWk.reset();
			}
		} catch (IOException e) {
			LOG.debug("run : cannot register watch service");
		} catch (InterruptedException e) {
			LOG.debug("run : Thread interrupted");
		}
		interruptListenerThread();
	}
	
	protected abstract void interruptListenerThread();
}
