package pl.com.tt.logger.file;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

import org.apache.log4j.Logger;

import javafx.scene.control.TextArea;
import pl.com.tt.logger.action.Action;
import pl.com.tt.logger.action.ActionFactory;
import pl.com.tt.logger.action.invoker.ActionInvoker;
import pl.com.tt.logger.file.reader.ChangesFileReader;
import pl.com.tt.logger.file.util.FileUtil;
import pl.com.tt.logger.gui.Displayable;
import pl.com.tt.logger.loggerwatchservice.factory.WatchServiceFactory;
import pl.com.tt.logger.model.LogBlockWrapper;
import pl.com.tt.logger.model.factory.ModelFactory;
import pl.com.tt.logger.util.LogUtilities;

public class FileChangeListener extends Thread{
	
	private static final Logger LOG= Logger.getLogger(FileChangeListener.class);
	
	//properties
	private String filename;
	private File observedFile;
	private WatchService watchService;
	private Displayable displayArea;
	
	//
	private WatchServiceFactory watchServiceFactory;
	private ChangesFileReader changesFileReader;
	private FileUtil fileUtil;
	private ModelFactory modelFactory;
	private ActionInvoker actionInvoker;
	private ActionFactory actionFactory;
	
	private FileChangeListener() {
		watchServiceFactory= new WatchServiceFactory();
		fileUtil= new FileUtil();
		modelFactory= new ModelFactory();
		actionFactory= new ActionFactory();
	}
	
	public FileChangeListener(String filePath, ActionInvoker actionInvoker, Displayable displayArea) throws IOException {
		this();
		this.displayArea= displayArea;
		this.actionInvoker= actionInvoker;
		this.filename= filePath;
		observedFile= new File(filePath);
		if(!observedFile.exists()){
			throw new FileNotFoundException(String.format("Cannot find file %s", filePath));
		}
		changesFileReader= new ChangesFileReader(filePath);
		watchService= watchServiceFactory.getWatchService();
	}
	
	private void handleChanges() throws InterruptedException{
		String line="";
		
		try{
			while(!(line= changesFileReader.getLine()).isEmpty()){
				LogBlockWrapper log= modelFactory.getLogBlock(line);
				Action action= actionFactory.getDisplayLogAction(displayArea, log);
				LOG.debug(String.format("handleChanges : try add action"));
				actionInvoker.addAction(action);
			}
		}catch(IOException e){
			LOG.error(String.format("handleChanges : Cannot read line %s\n%s", e, LogUtilities.stackTraceAsString(e)));
		}
	}
	
	private void observerEvents(WatchKey watchKey) throws InterruptedException{
		for(WatchEvent<?> event : watchKey.pollEvents()){
			Path path= (Path) event.context();
			LOG.debug(String.format("observerEvents : File changed %s", path));
			if(path.toString().equals(observedFile.getName())){
				handleChanges();
				LOG.debug("observerEvents : handle change");
			}
		}
	}
	
	@Override
	public void run() {
		Path fileDir= fileUtil.getFileDirPath(filename);
		try{
			WatchKey watchKey= fileDir.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY);
			while(!Thread.currentThread().isInterrupted()){
				WatchKey localWk= watchService.take();
				observerEvents(localWk);
				localWk.reset();
			}
		}catch(IOException e){
			LOG.error(String.format("FileChangeListener : Occured exception %s for FileChangeListerner, file=%s\n%s", 
					e, filename, LogUtilities.stackTraceAsString(e)));
		}catch(InterruptedException e){
			LOG.info(String.format("FileChangeListener : thred stopped for file %s", filename));
		}finally {
			changesFileReader.close();
		}
	}
	
	//getters and setters
	public String getFilename() {
		return filename;
	}
}
