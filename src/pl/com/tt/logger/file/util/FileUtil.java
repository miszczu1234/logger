package pl.com.tt.logger.file.util;

import java.nio.file.FileSystems;
import java.nio.file.Path;

public class FileUtil {
	public Path getFileDirPath(String filepath){
		Path pathToFile= FileSystems.getDefault().getPath(filepath);
		Path pathToFileDir= pathToFile.getParent();
		
		return pathToFileDir;
	}
}
