package pl.com.tt.logger.file;

import java.awt.Event;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import pl.com.tt.logger.action.invoker.ActionInvoker;
import pl.com.tt.logger.file.refresh.Touch;
import pl.com.tt.logger.gui.Displayable;
import pl.com.tt.logger.util.LogUtilities;

public class LogDirectoryChangeListener extends ChangeListener{
	
	private static final Logger LOG= Logger.getLogger(LogDirectoryChangeListener.class);
	
	private static final String TMP_FILENAME = "logger.tmp";
	
	private static final ArrayList<Kind<Path>> EVENTS= 
			new ArrayList<Kind<Path>>(Arrays.asList(StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE,
					StandardWatchEventKinds.ENTRY_MODIFY));
	
	private File directory;
	private Map<String, LogFileChangeListener> observedLogFiles;
	private List<Path> logFiles; 
	private ActionInvoker actionInvoker;
	private Displayable displayable;
	private Touch refreshThread;
	
	public LogDirectoryChangeListener(String filename, ActionInvoker actionInvoker, Displayable displayable) throws IOException {
		super(filename, EVENTS);
		logFiles= Collections.synchronizedList(new ArrayList<Path>());
		directory= new File(filename);
		createTmpFile(directory);
		this.actionInvoker= actionInvoker;
		this.displayable= displayable;
		observedLogFiles= new HashMap<>();
		startRefreshThread(logFiles);
	}

	private void createTmpFile(File directory){
		Path tmpFilePath= FileSystems.getDefault().getPath(directory.getAbsolutePath(), TMP_FILENAME);
		File tmpFile= new File(tmpFilePath.toString());
		if(!tmpFile.exists()){
			try{
				tmpFile.createNewFile();
			}catch(IOException e){
				LOG.error(String.format("createTmpFile : Cannot create tmp file %s", tmpFile.getAbsolutePath()));
			}
		}
		logFiles.add(FileSystems.getDefault().getPath(tmpFile.getAbsolutePath()));
	}
	
	private void startRefreshThread(List<Path> filesToRefresh){
		refreshThread= new Touch(filesToRefresh);
		refreshThread.start();
	}
	
	private void newLogFileObserver(String filename) throws IOException{
		Path logPath= FileSystems.getDefault().getPath(directory.getAbsolutePath());
		List<Kind<Path>> events= new ArrayList<>();
		events.add(StandardWatchEventKinds.ENTRY_MODIFY);
		LogFileChangeListener logFileListener= new LogFileChangeListener(filename, logPath, events, 
				actionInvoker, displayable);
		observedLogFiles.put(filename, logFileListener);
		logFileListener.start();
		logFiles.add(FileSystems.getDefault().getPath(logPath.toAbsolutePath().toString(), filename));
	}
	
	@Override
	protected void handleEvent(WatchEvent<?> eventType, Path context) throws InterruptedException {
		
		Kind<?> eventKind= eventType.kind();
		String filename= context.getFileName().toString();
		
		if(eventKind.equals(StandardWatchEventKinds.ENTRY_CREATE)
				|| (eventKind.equals(StandardWatchEventKinds.ENTRY_MODIFY) && (!observedLogFiles.containsKey(filename))
						&& (!filename.equals(TMP_FILENAME)))){
			try{
				LOG.debug(String.format("handleEvent : try register new log file listener, file %s", filename));
				newLogFileObserver(filename);
			}catch(IOException e){
				LOG.error(String.format("handleEvent : Cannot register new log file listener, context %s\n%s", 
						context, LogUtilities.stackTraceAsString(e)));
			}
		}else if(eventKind.equals(StandardWatchEventKinds.ENTRY_DELETE)){
			LOG.debug(String.format("handleEvent : try unregister log file listener, file %s", filename));
			LogFileChangeListener newFileChangeListener= observedLogFiles.remove(filename);
			newFileChangeListener.interrupt();
			LOG.debug(String.format("handleEvent : log file listener unregisterd, file %s", filename));
		}
	}

	@Override
	protected void interruptListenerThread() {
		refreshThread.interrupt();
		for(LogFileChangeListener logChangeThread : observedLogFiles.values()){
			logChangeThread.interrupt();
		}
	}
}
