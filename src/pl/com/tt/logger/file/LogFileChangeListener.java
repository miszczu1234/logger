package pl.com.tt.logger.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import pl.com.tt.logger.action.Action;
import pl.com.tt.logger.action.ActionFactory;
import pl.com.tt.logger.action.invoker.ActionInvoker;
import pl.com.tt.logger.file.reader.ChangesFileReader;
import pl.com.tt.logger.gui.Displayable;
import pl.com.tt.logger.model.LogBlockWrapper;
import pl.com.tt.logger.model.factory.ModelFactory;
import pl.com.tt.logger.util.LogUtilities;

public class LogFileChangeListener extends ChangeListener{

	private static final Logger LOG= Logger.getLogger(LogFileChangeListener.class);
	private static final String LOG_BLOCK_REGEX = "^(?:[0-9][0-9])?[0-9][0-9]-[0-3][0-9]-[0-3][0-9]( )[0-9][0-9]:[0-9][0-9]:[0-9][0-9](,)[0-9][0-9][0-9]( )(INFO|ERROR|DEBUG|WARN).*";
	private static final Pattern LOG_BLOCK_PATTERN= Pattern.compile(LOG_BLOCK_REGEX);
	
	private File observedFile;
	private Displayable displayable;
	private ChangesFileReader changesFileReader;
	private ModelFactory modelFactory;
	private ActionInvoker actionInvoker;
	private ActionFactory actionFactory;
	
	public LogFileChangeListener(String filename, Path path, List<Kind<Path>> events, 
			ActionInvoker actionInvoker, Displayable displayable) throws IOException {
		super(path.toString(), events);
		modelFactory= new ModelFactory();
		actionFactory= new ActionFactory();
		this.actionInvoker= actionInvoker;
		this.displayable= displayable;
		Path observedFilePath= FileSystems.getDefault().getPath(path.toString(), filename);
		observedFile= new File(observedFilePath.toString());
		if(!observedFile.exists()){
			throw new FileNotFoundException(String.format("Cannot find file %s", observedFile.getAbsolutePath()));
		}
		changesFileReader= new ChangesFileReader(observedFile.getAbsolutePath());
	}

	private boolean isLogBlock(String line){
		Matcher matcher= LOG_BLOCK_PATTERN.matcher(line);
		return matcher.matches();
	}
	
	private List<LogBlockWrapper> getLogBlocks(String line){
		
		List<LogBlockWrapper> logBlocks= new LinkedList<>();
		BufferedReader lineReader= new BufferedReader(new StringReader(line));
		String singleLine= "";
		
		try{
			while((singleLine= lineReader.readLine())!=null){
				if(singleLine.isEmpty()){
					continue;
				}
				LOG.debug(String.format("getLogBlocks : got line '%s'", singleLine));
				if((!isLogBlock(singleLine)) && (!logBlocks.isEmpty())){
					LogBlockWrapper currentBlock= logBlocks.get(logBlocks.size()-1);
					currentBlock.appendText(singleLine);
					continue;
				}
				LogBlockWrapper log= modelFactory.getLogBlock(singleLine);
				logBlocks.add(log);
			}
		}catch(IOException e){
			LOG.error(String.format("getLogBlocks : cannot parse line %s\n%s", line, LogUtilities.stackTraceAsString(e)));
		}
		
		return logBlocks;
	}
	
	private void displayLogs(List<LogBlockWrapper> logs) throws InterruptedException{
		for(LogBlockWrapper log : logs){
			Action action= actionFactory.getDisplayLogAction(displayable, log);
			actionInvoker.addAction(action);
		}
	}
	
	@Override
	protected void handleEvent(WatchEvent<?> eventType, Path context) throws InterruptedException {
		
		if(!context.toString().equals(observedFile.getName())){
			return;
		}
		
		String line="";
		try{
			while(!(line= changesFileReader.getLine()).isEmpty()){
				List<LogBlockWrapper> logs= getLogBlocks(line);
				displayLogs(logs);
			}
		}catch(IOException e){
			LOG.error(String.format("handleChanges : Cannot read line %s\n%s", e, LogUtilities.stackTraceAsString(e)));
		}
	}

	@Override
	protected void interruptListenerThread() {
		
	}

}
