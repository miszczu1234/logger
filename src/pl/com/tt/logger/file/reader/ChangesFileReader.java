package pl.com.tt.logger.file.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;

import org.apache.log4j.Logger;

import pl.com.tt.logger.util.LogUtilities;

public class ChangesFileReader {
	
	private static final Logger LOG = Logger.getLogger(ChangesFileReader.class);
	
	private File file;
	private BufferedReader bufferedReader;
	
	public ChangesFileReader(String filename) throws IOException {
		file= new File(filename);
		FileInputStream fileInpStream= new FileInputStream(filename);
		fileInpStream.skip(file.length());
		bufferedReader= new BufferedReader(new InputStreamReader(fileInpStream));
	}
	
	public String getLine() throws IOException{
		String line = "";
		String tmpLine= "";
		while((tmpLine= bufferedReader.readLine())!=null){
			line+=tmpLine+"\n";
		}
		
		bufferedReader.mark(0);
		
		return line;
	}
	
	public void close(){
		try{
			if(bufferedReader!=null){
				bufferedReader.close();
			}
		}catch (IOException e){
			LOG.error(String.format("close : Cannot close stream, occured exception %s\n%s", e, LogUtilities.stackTraceAsString(e)));
		}
	}
}
