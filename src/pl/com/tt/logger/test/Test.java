package pl.com.tt.logger.test;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Test {
	
	private static void testPattern(){
		final String regex = "^(?:[0-9][0-9])?[0-9][0-9]-[0-3][0-9]-[0-3][0-9]( )[0-9][0-9]:[0-9][0-9]:[0-9][0-9](,)[0-9][0-9][0-9]( )(INFO|ERROR|DEBUG|WARN).*";
		Pattern pattern= Pattern.compile(regex);
		String str= "2016-04-11 15:21:21,186 INFO  [main] wt.pom.properties  - Connection: datastore=Oracle(Oracle Database 11g Enterprise Edition Release 11.2.0.4.0 - 64bit Production) jdbcVersion=11.2 driver=Oracle JDBC driver(11.2.0.4.0) driverVersion=11.2";
		System.out.println(str);
		Matcher matcher= pattern.matcher(str);
		System.out.println(matcher.matches());
	}
	
	public static void main(String args[]) throws IOException, InterruptedException{
		testPattern();
	}
}
