package pl.com.tt.logger.view.factory;

import java.io.IOException;
import java.net.URL;

import javafx.fxml.FXMLLoader;

public class ContainersFactory {
	
	@SuppressWarnings("unchecked")
	public <T> T getContainer(String viewPath) throws IOException{
		URL viewUrl= getClass().getClassLoader().getResource(viewPath);
		FXMLLoader loader= new FXMLLoader(viewUrl);
		T root= (T)FXMLLoader.load(viewUrl);
		
		return root;
	}
}
