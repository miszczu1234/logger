package pl.com.tt.logger.view.factory;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

public class AlertFactory {
	public static Alert getAlert(AlertType type, String text, String header, ButtonType... buttons){
		Alert alert= new Alert(type, text, buttons);
		alert.setHeaderText(header);
		
		return alert;
	}
}
