package pl.com.tt.logger.view.config.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="preference")
@XmlAccessorType(XmlAccessType.FIELD)
public class PreferenceModel {
	
	@XmlAttribute
	private String  name;

	@XmlAttribute 
	private String view;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getView() {
		return view;
	}

	public void setView(String view) {
		this.view = view;
	}
}
