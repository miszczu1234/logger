package pl.com.tt.logger.view.config.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="View")
@XmlAccessorType(XmlAccessType.FIELD)
public class ViewModel {
	
	@XmlElement(name="Preference")
	private List<PreferenceModel> preferences;

	public ViewModel() {
		preferences= new LinkedList<>();
	}
	
	public List<PreferenceModel> getPreferences() {
		return preferences;
	}

	public void setPreferences(List<PreferenceModel> preferences) {
		this.preferences = preferences;
	}
	
	public List<String> getPreferencesNames(){
		
		List<String> prefValues= new LinkedList<>();
		
		for(PreferenceModel preference : preferences){
			String prefName= preference.getName();
			prefValues.add(prefName);
		}
		
		return prefValues;
	}

	@Override
	public String toString() {
		return "ViewModel [preferences=" + Arrays.toString(preferences.toArray()) + "]";
	}
	
	
}
