package pl.com.tt.logger.view.config;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;

import pl.com.tt.logger.config.Paths;
import pl.com.tt.logger.util.LogUtilities;
import pl.com.tt.logger.view.config.model.ViewModel;
import pl.com.tt.logger.xml.XmlUnmarshaller;

public class ViewHandler {
	
	private static final Logger LOG= Logger.getLogger(ViewHandler.class); 
	private static final String XML_EXT = "xml";
	
	private XmlUnmarshaller unmarshaller;
	private Map<String, File> views;
	
	public ViewHandler() {
		views= new HashMap<String, File>();
		unmarshaller= new XmlUnmarshaller();
		views= loadViewFiles(new File(Paths.VIEW_PATH));
	}
	
	private boolean isViewFile(File file){
		
		if(file.isDirectory()){
			return false;
		}
		
		String filename= file.getName();
		String[] filenameArray= filename.split("\\.");
		boolean isXml= filenameArray[filenameArray.length -1].equals(XML_EXT);
		
		return isXml;
	}
	
	private Map<String, File> loadViewFiles(File viewDir){
		Map<String, File> xmlFiles= new HashMap<>();
		if(!viewDir.exists()){
			return xmlFiles;
		}
		File[] filesInDir= viewDir.listFiles();
		
		for(File file : filesInDir){
			if(file.isDirectory()){
				Map<String, File> childXmlFiles= loadViewFiles(file);
				xmlFiles.putAll(childXmlFiles);
			}else if(isViewFile(file)){
				String key= file.getName().split("\\.")[0];
				xmlFiles.put(key, file);
				LOG.debug(String.format("loadViewFiles : file %s placed with key %s", file.getName(), key));
			}
		}
		
		return xmlFiles;
	}
	
	public ViewModel getModel(String name){
		ViewModel viewModel= null;
		
		try{
			LOG.debug(String.format("getModel : Searching file %s", name));
			File xmlViewDef= views.get(name);
			if(xmlViewDef!=null){
				viewModel= unmarshaller.unmarshall(xmlViewDef, ViewModel.class);
			}
		}catch(JAXBException e){
			LOG.error(String.format("getModel : cannot unmarsahll file for view %s\n%s", name, LogUtilities.stackTraceAsString(e)));
		}
		
		return viewModel;
	}
}
