package pl.com.tt.logger.view.manager;

import java.io.IOException;
import java.net.URL;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ViewStarter {
	public Stage showView(String viewPath, String title) throws IOException{
		URL viewUrl= getClass().getClassLoader().getResource(viewPath);
		Parent root= FXMLLoader.load(viewUrl);
		Stage stage= new Stage();
		stage.setTitle(title);
		stage.setScene(new Scene(root));
		stage.show();
		
		return stage;
	}
	
	public Stage showView(Stage primaryStage, String viewPath) throws IOException {
		URL resourceLocation= getClass().getClassLoader().getResource(viewPath);
		Parent root= FXMLLoader.load(resourceLocation);
		Scene scene= new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.show();
		
		return primaryStage;
	}
}
