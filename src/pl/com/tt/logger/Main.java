package pl.com.tt.logger;
	
import javafx.application.Application;
import javafx.stage.Stage;
import pl.com.tt.logger.config.Views;
import pl.com.tt.logger.view.manager.ViewStarter;


public class Main extends Application {
	
	@Override
	public void start(Stage primaryStage) {
		try {
			new ViewStarter().showView(primaryStage, Views.MAIN_VIEW);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
