package pl.com.tt.logger.config;

public class Views {
	public static final String MAIN_VIEW= "pl/com/tt/logger/view/MainWindow.fxml";
	public static final String SETTINGS_VIEW= "pl/com/tt/logger/view/Settings.fxml";
}
