package pl.com.tt.logger.property;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.log4j.Logger;

import pl.com.tt.logger.config.Paths;
import pl.com.tt.logger.util.LogUtilities;

public class PropertyHandler {
	
	private static final Logger LOG= Logger.getLogger(PropertyHandler.class);
	
	private static final String PROPERTY_FILE_PATH= Paths.PROPERTY_PATH;
	
	private static PropertyHandler instance;
	private Properties properties;
	
	private HashMap<String, Object> tmpProperties;
	
	private File propFile;
	private InputStream propertyFileInputStream;
	
	public PropertyHandler() {
		try{
			tmpProperties= new HashMap<>();
			propFile= new File(PROPERTY_FILE_PATH);
			propertyFileInputStream= new FileInputStream(propFile);
			properties= new Properties();
			properties.load(propertyFileInputStream);
		}catch(IOException e){
			LOG.error(String.format("Cannot load properties from path %s", PROPERTY_FILE_PATH));
		}
	}
	
	public static PropertyHandler getInstance(){
		if(instance==null){
			instance= new PropertyHandler();
		}
		
		return instance;
	}
	
	public void refreshProperties(){
		try{
			properties.load(propertyFileInputStream);
		}catch(IOException e){
			LOG.debug(String.format("refreshProperties : cannot refresh properties\n%s", LogUtilities.stackTraceAsString(e)));
		}
	}
	
	public void saveProperties(){
		try{
			FileOutputStream propFileOut= new FileOutputStream(propFile);
			properties.store(propFileOut, null);
			propFileOut.flush();
			propFileOut.close();
		}catch(IOException e){
			LOG.debug(String.format("saveProperties : cannot save properties\n%s", LogUtilities.stackTraceAsString(e)));
		}
	}
	
	public String getProperty(String key){
		return properties.getProperty(key);
	}
	
	public String getProperty(PropertyList key){
		if(tmpProperties.containsKey(key.getName())){
			return tmpProperties.get(key.getName()).toString();
		}
		return properties.getProperty(key.getName());
	}
	
	public Object setTmpProperty(String key, Object value){
		return tmpProperties.put(key, value);
	}
	
	public Object setTmpProperty(PropertyList key, Object value){
		return tmpProperties.put(key.getName(), value);
	}
	
	public void clearTmpProperties(){
		tmpProperties.clear();
	}
	
	public void saveTmpProperties(){
		Iterator<Entry<String, Object>> entryIt= tmpProperties.entrySet().iterator();
		while(entryIt.hasNext()){
			Entry<String, Object> entry= entryIt.next();
			String key =entry.getKey();
			String value= (String) entry.getValue();
			setProperty(key, value);
		}
		saveProperties();
		tmpProperties.clear();
	}
	
	public Object setProperty(String key, String value){
		LOG.debug(String.format("setProperty : Property %s changed on %s", key, value));
		return properties.setProperty(key, value);
	}
	
	public Object setProperty(PropertyList key, String value){
		return properties.setProperty(key.getName(), value);
	}
}
