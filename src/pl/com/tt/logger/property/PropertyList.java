package pl.com.tt.logger.property;

public enum PropertyList {
	
	COLOR_DEBUG("font.color.DEBUG"),
	COLOR_ERROR("font.color.ERROR"),
	COLOR_INFO("font.color.INFO"),
	COLOR_WARN("font.color.WARN"),
	COLOR_FATAL("font.color.FATAL"),
	FONT_SIZE_MIN("font.size.min"),
	FONT_SIZE_MAX("font.size.max"),
	FONT_SIZE_DEFAULT("font.size.default");
	
	private String name;
	
	private PropertyList(String propValue) {
		this.name= propValue;
	}

	public String getName() {
		return name;
	}
}
