package pl.com.tt.logger.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class LogUtilities {
	public static String stackTraceAsString(Exception ex){
		String stackTrace="";
		PrintWriter printWriter= null;
		try{
			StringWriter strWriter= new StringWriter();
			printWriter= new PrintWriter(strWriter);
			ex.printStackTrace(printWriter);
			stackTrace= strWriter.toString();
		}catch(Exception e){
			if(printWriter!=null){
				printWriter.close();
			}
		}
		
		
		return stackTrace;
	}
}
