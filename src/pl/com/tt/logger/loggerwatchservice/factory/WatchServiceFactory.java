package pl.com.tt.logger.loggerwatchservice.factory;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.WatchService;

import org.apache.log4j.Logger;

import pl.com.tt.logger.file.util.FileUtil;

public class WatchServiceFactory {
	
	private static final Logger LOG = Logger.getLogger(WatchServiceFactory.class);
	
	private FileUtil fileUtil;
	
	public WatchServiceFactory() {
		fileUtil= new FileUtil();
	}
	
	public WatchService getWatchService() throws IOException {
		WatchService watchService= FileSystems.getDefault().newWatchService();
		
		return watchService;
	}
}
